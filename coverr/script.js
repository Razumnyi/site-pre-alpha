<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Style-Type" content="text/css">
  <title>Document</title>
  <meta name="Generator" content="Cocoa HTML Writer">
  <meta name="CocoaVersion" content="1404.13">
  <style type="text/css">
    p.p1 {margin: 0.0px 0.0px 0.0px 0.0px; line-height: 14.0px; font: 12.0px Courier; color: #000000; -webkit-text-stroke: #000000}
    span.s1 {font-kerning: none}
  </style>
</head>
<body>
<p class="p1"><span class="s1">//jQuery is required to run this code $( document ).ready(function() { scaleVideoContainer(); initBannerVideoSize('.video-container .poster img'); initBannerVideoSize('.video-container .filter'); initBannerVideoSize('.video-container video'); $(window).on('resize', function() { scaleVideoContainer(); scaleBannerVideoSize('.video-container .poster img'); scaleBannerVideoSize('.video-container .filter'); scaleBannerVideoSize('.video-container video'); }); }); function scaleVideoContainer() { var height = $(window).height() + 5; var unitHeight = parseInt(height) + 'px'; $('.homepage-hero-module').css('height',unitHeight); } function initBannerVideoSize(element){ $(element).each(function(){ $(this).data('height', $(this).height()); $(this).data('width', $(this).width()); }); scaleBannerVideoSize(element); } function scaleBannerVideoSize(element){ var windowWidth = $(window).width(), windowHeight = $(window).height() + 5, videoWidth, videoHeight; console.log(windowHeight); $(element).each(function(){ var videoAspectRatio = $(this).data('height')/$(this).data('width'); $(this).width(windowWidth); if(windowWidth &lt; 1000){ videoHeight = windowHeight; videoWidth = videoHeight / videoAspectRatio; $(this).css({'margin-top' : 0, 'margin-left' : -(videoWidth - windowWidth) / 2 + 'px'}); $(this).width(videoWidth).height(videoHeight); } $('.homepage-hero-module .video-container video').addClass('fadeIn animated'); }); }</span></p>
</body>
</html>
